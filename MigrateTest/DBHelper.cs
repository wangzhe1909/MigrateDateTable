﻿using iAnywhere.Data.SQLAnywhere;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 

namespace MigrateTest
{
    public class DBHelper
    { 
        #region 构造函数
        public DBHelper(string ConnStr)
        {
            connstring = ConnStr;
        }
      
        public DBHelper(string Host, string Server, string UserId, string Password)
        {
            connstring = "host=" + Host + ";server=" + Server + ";userid=" + UserId + ";password=" + Password + ";";
        }

        public DBHelper()
        {

        } 
        #endregion  

        #region 属性信息 
        private string connstring = null;
        public string ConnStr
        {
            set
            {
                connstring = value;
            }
            get
            {
                return connstring;
            }
        }
        #endregion 

        public  DataSet QueryOdbc(string SqlString)
        {
            using (SAConnection conn = new SAConnection(connstring))
            {
                SACommand cmd = new SACommand(SqlString, conn);
                try
                { 
                    conn.Open();
                    SADataAdapter adp = new SADataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adp.Fill(ds);
                    conn.Close();
                    return ds;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="SQLStringList"></param>
        public  void ExecuteSqlTran(List<string> SQLStringList)
        {
            using (SAConnection conn = new SAConnection(connstring))
            {
                conn.Open();
                SACommand cmd = new SACommand
                {
                    Connection = conn
                };
                SATransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int i = 0; i < SQLStringList.Count; i++)
                    {
                        string strsql = SQLStringList[i].ToString();
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="SQLStringList"></param>
        public  void ExecuteSqlTran(string SQLString)
        {
            using (SAConnection conn = new SAConnection(connstring))
            {
                conn.Open();
                SACommand cmd = new SACommand
                {
                    Connection = conn
                };
                SATransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    if (SQLString.Trim().Length > 1)
                    {
                        cmd.CommandText = SQLString;
                        cmd.ExecuteNonQuery();
                    }
                    tx.Commit();
                }
                catch (Exception E)
                {
                    tx.Rollback();
                    throw new Exception(E.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        } 

        /// <summary>
        /// 获取最大主键ID
        /// </summary>
        /// <param name="SqlString"></param>
        /// <returns></returns>
        public   int OdbcGetMaxPKID(string SqlString)
        {
            using (SAConnection conn = new SAConnection(connstring))
            {
                SACommand cmd = new SACommand(SqlString, conn);
                try
                {
                    conn.Open();
                    int max_id = cmd.ExecuteScalar().ToString().Equals("") ? 0 : int.Parse(cmd.ExecuteScalar().ToString());
                    conn.Close();
                    return max_id;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        ///// <summary>
        ///// 测试连接
        ///// </summary>
        //public static void TestConn()
        //{
        //    using (SAConnection Conn = new SAConnection(connstring))
        //    {
        //        Conn.Open();
        //        SACommand cmd = new SACommand();
        //        cmd.Connection = Conn;
        //        Conn.Close();
        //    }
        //}
    }
}
