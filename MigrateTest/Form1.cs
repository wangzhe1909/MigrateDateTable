﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MigrateTest
{
    public partial class Form1 : Form
    {
        DBHelper SourceDb = new DBHelper("192.168.11.197", "pos1101", "dba", "sql");//源数据库
        DBHelper TargetDb = new DBHelper("127.0.0.1", "pos012", "dba", "sql"); //目标

        string Serverpartshop_id = "882";//是获取的，此处暂时默认值为882
        string Serverpartcode = "888888";//可以获取
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<string> list = new List<string>();
            List<string> _list = new List<string>();
            decimal _MaxID = decimal.Parse(SourceDb.QueryOdbc("select max(dataupdate_id) from t_dataupdate").Tables[0].Rows[0][0].ToString());
            decimal _TargetMaxID = decimal.Parse(SourceDb.QueryOdbc("select max(endaccount_id) from t_endaccount_new").Tables[0].Rows[0][0].ToString());
            try
            {
                string sql_endaccount = " select endaccount_id,endaccount_code,serverpartcode,shopcode,machinecode,endperson_code ,startdate,enddate,ticket_count,total_count,total_amount," +
              "total_offamount,cashpay,bankpay,memberpay ,couponpay,mobilepay,internalpay,contribution_amount,payment_details,endaccount_type,endaccount_desc " +
                " from t_endaccount_new a where a.enddate is not null and not EXISTS (select 1 from t_dataupdate b where a.endaccount_id = b.data_id and a.enddate = b.data_date and b.data_type =1002)";

                string sql_personsell = "select personsell_id, endaccount_code,serverpartcode,shopcode,machinecode,startdate,enddate,sellworker_code,ticket_count,total_count,total_amount," +
                " total_offamount,cashpay,bankpay,memberpay,couponpay,mobilepay,internalpay,otherpay,contribution_amount,payment_details,woker_number,personsell_desc " +
                " from t_personsell_new a where a.enddate is not null and not EXISTS (select 1 from t_dataupdate b where a.personsell_id = b.data_id and a.enddate = b.data_date and b.data_type =2002)";

                string sql_commoditysale = "select commoditysale_id,endaccount_code,commodity_type,commodity_code,commodity_barcode,commodity_name,tickte_count,total_count," +
                " total_amount,total_offamount,woker_number,create_date,commoditysale_desc from t_commoditysale_new  a where create_date is not null and " +
                " not EXISTS (select 1 from t_dataupdate b where a.commoditysale_id = b.data_id and a.create_date = b.data_date and b.data_type =3002)";

                string sql_sellmaster = "select  sellmaster_id,sellmaster_code,serverpartcode,serverpart_name,shopcode,shopname,machinecode,sellworker_code,sellworker_name,sellmaster_date,ticket_code," +
                "sellmaster_count,sellmaster_offprice,sellmaster_amount,cashpay,bankpay,memberpay,couponpay,mobilepay,internalpay,otherpay,pay_amount,payment_type,coupon_type,mobilepay_code," +
                "merchant_order,sellmaster_desc from t_sellmaster  a where sellmaster_date is not null and  " +
                " not EXISTS (select 1 from t_dataupdate b where a.sellmaster_id = b.data_id and a.sellmaster_date = b.data_date and b.data_type =2002)";

                string sql_selldetails = "select  selldetails_id, sellmaster_code,commodity_code, commodity_barcode, commodity_name,selldetails_count ,selldetails_price, selldetails_offprice,selldetails_amount," +
                "linenum,payment_type, create_date,selldetails_desc from t_selldetails  a where a.create_date is not null and " +
                " not EXISTS (select 1 from t_dataupdate b where a.selldetails_id = b.data_id and a.create_date = b.data_date and b.data_type =5002)";
                DataTable dt_endaccount = SourceDb.QueryOdbc(sql_endaccount).Tables[0];
                DataTable dt_personsell = SourceDb.QueryOdbc(sql_personsell).Tables[0];
                DataTable dt_commoditysale = SourceDb.QueryOdbc(sql_commoditysale).Tables[0];
                DataTable dt_sellmaster = SourceDb.QueryOdbc(sql_sellmaster).Tables[0];
                DataTable dt_selldetails = SourceDb.QueryOdbc(sql_selldetails).Tables[0];
                //拷贝表数据,成功后插入上传数据记录 

                if (dt_endaccount.Rows.Count > 0)
                {
                    InsertTableData(dt_endaccount, "t_endaccount_new", TargetDb);
                    InsertLog(dt_endaccount, "t_endaccount_new", TargetDb);
                }
                if (dt_commoditysale.Rows.Count > 0)
                {
                    InsertTableData(dt_commoditysale, "t_commoditysale_new", TargetDb);
                    InsertLog(dt_commoditysale, "t_commoditysale_new", TargetDb);
                }
                if (dt_personsell.Rows.Count > 0)
                {
                    InsertTableData(dt_personsell, "t_personsell_new", TargetDb);
                    InsertLog(dt_personsell, "t_personsell_new", TargetDb);
                }
                if (dt_sellmaster.Rows.Count > 0)
                {
                    InsertTableData(dt_sellmaster, "t_sellmaster", TargetDb);
                    InsertLog(dt_sellmaster, "t_sellmaster", TargetDb);
                }
                if (dt_selldetails.Rows.Count > 0)
                {
                    InsertTableData(dt_selldetails, "t_selldetails", TargetDb);
                    InsertLog(dt_selldetails, "t_selldetails", TargetDb);
                }
                MessageBox.Show("上传成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region 方法 -> 类型匹配
        /// <summary>
        /// 类型匹配
        /// </summary>
        /// <param name="type">类型</param>
        /// <param name="typeName">类型名称</param>
        /// <returns></returns>
        public static bool IsType(Type type, string typeName)
        {
            if (type.ToString() == typeName)
                return true;
            if (type.ToString() == "System.Object")
                return false;
            return IsType(type.BaseType, typeName);
        }
        #endregion

        #region 上传时重构目标数据表insert（数据中带null的处理）
        public void InsertTableData(DataTable dt, string TableName, DBHelper dbhelper)
        {
            try
            {
                List<string> _List = new List<string>();
                if (dt == null)
                {
                    throw new Exception("datatable不可为空！");
                }
                if (string.IsNullOrEmpty(TableName))
                {
                    throw new Exception("表名不可为空！");
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string sqlstring = string.Empty;
                    string sqlString = "insert into " + TableName + " ({0}) VALUES ({1})";
                    string Names = string.Empty;
                    string values = string.Empty;
                    foreach (DataColumn c in dt.Columns)
                    {
                        if (dt.Rows[i][c.ColumnName] != null && !string.IsNullOrEmpty(dt.Rows[i][c.ColumnName].ToString()))
                        {
                            Names += (string.IsNullOrEmpty(Names) ? "" : ",") + c.ColumnName;
                            if (IsType(c.DataType, "System.Nullable`1[System.Int16]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Int32]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Int64]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Double]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Decimal]") ||
                               c.DataType == typeof(System.Int16) ||
                               c.DataType == typeof(System.Int32) ||
                                c.DataType == typeof(System.Int64) ||
                                c.DataType == typeof(System.Double) ||
                                c.DataType == typeof(System.Decimal))
                            {
                                values += (string.IsNullOrEmpty(values) ? "" : ",") + dt.Rows[i][c.ColumnName].ToString();
                            }

                            if (IsType(c.DataType, "System.Nullable`1[System.DateTime]") ||
                               c.DataType == typeof(System.DateTime))
                            {
                                values += (string.IsNullOrEmpty(values) ? "" : ",") + "DateTime('" + dt.Rows[i][c.ColumnName].ToString() + "')";
                            }
                            if (IsType(c.DataType, "System.String") || c.DataType == typeof(System.String))
                            {
                                values += (string.IsNullOrEmpty(values) ? "" : ",") + "'" + dt.Rows[i][c.ColumnName].ToString() + "'";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(Names) && !string.IsNullOrEmpty(values))
                    {
                        sqlstring = string.Format(sqlString, Names, values);
                        _List.Add(sqlstring);
                    }
                }
                dbhelper.ExecuteSqlTran(_List);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 上传成功重构记录表insert
        public void InsertLog(DataTable dt, string TableName,DBHelper dbhelper)
        {
            try
            {
                List<string> list = new List<string>();
                decimal _MaxID = 0;
                if (dt == null)
                {
                    throw new Exception("datatable不可为空！");
                } 
                decimal DataType = 0; 
                string serverpartcode=string.Empty;
                decimal DataId = 0;
                DateTime? Datadate; Datadate = null;
                string ID = TargetDb.QueryOdbc("select max(dataupdate_id) from t_dataupdate").Tables[0].Rows[0][0].ToString();
                if (!string.IsNullOrEmpty(ID))
                {
                    _MaxID = decimal.Parse(ID)+1;
                }
                else
                {
                    _MaxID = 0;
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (TableName == "t_endaccount_new")
                    {
                        DataType = 1002;//日结
                        serverpartcode = dt.Rows[i]["serverpartcode"].ToString();
                        DataId = decimal.Parse(dt.Rows[i]["endaccount_id"].ToString());
                        Datadate=Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());
                    }
                    else if (TableName == "t_commoditysale_new")
                    {
                        DataType = 3002;//单品
                        serverpartcode = Serverpartcode;//不能获取，直接截取
                        DataId = decimal.Parse(dt.Rows[i]["commoditysale_id"].ToString());
                        Datadate=Convert.ToDateTime(dt.Rows[i]["create_date"].ToString());
                    }
                    else if (TableName == "t_personsell_new")
                    {
                        DataType = 2002;//交办
                        serverpartcode=dt.Rows[i]["serverpartcode"].ToString();
                        DataId = decimal.Parse(dt.Rows[i]["personsell_id"].ToString());
                         Datadate=Convert.ToDateTime(dt.Rows[i]["ENDDATE"].ToString());
                    }
                    else if (TableName == "t_sellmaster")
                    {
                        DataType = 5001;// 流水
                        serverpartcode = dt.Rows[i]["serverpartcode"].ToString();
                        DataId = decimal.Parse(dt.Rows[i]["sellmaster_id"].ToString());
                        Datadate=Convert.ToDateTime(dt.Rows[i]["sellmaster_date"].ToString());
                    }
                    else if (TableName == "t_selldetails")
                    {
                        DataType = 5002;
                        serverpartcode = Serverpartcode;
                        DataId = decimal.Parse(dt.Rows[i]["selldetails_id"].ToString());
                        Datadate = Convert.ToDateTime(dt.Rows[i]["create_date"].ToString());
                    }

                    try
                    { 
                        string insertSql = "insert into t_dataupdate (dataupdate_id,serverpartcode,shopcode,machinecode,data_id,data_date,data_type,update_date,update_state,update_checknum,dataupdate_desc) " +
                          "values (" + (_MaxID++) + " ,'" + serverpartcode + "','" + dt.Rows[i]["shopcode"] + "','" + dt.Rows[i]["machinecode"] + "'," +
                         " " + DataId + ",DateTime('" + Datadate + "')," + DataType + ",DateTime('" + DateTime.Now + "'),0,'','')";
                        list.Add(insertSql);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                dbhelper.ExecuteSqlTran(list);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            //从目标数据表下载到本地(t_commodityex商品 t_sellworker收银员表  t_salespromote促销表)
            try
            {
                string sql_Targetsellworker = "select sellworker_id,serverpartcode,sellworkercode,sellworkername,prowerright,shopcode," +
                   " downloaddate,flag,valid,discount_rate,sellworkerpassword from t_sellworker where downloaddate is not null ";

                string sql_Soursellworker = "select sellworker_id,serverpartcode,sellworkercode,sellworkername,prowerright,shopcode," +
                   " downloaddate,flag,valid,discount_rate,sellworkerpassword from t_sellworker where downloaddate is not null ";

                DataTable dt_Soursellworker = SourceDb.QueryOdbc(sql_Targetsellworker).Tables[0];
                DataTable dt_Targetsellworker = TargetDb.QueryOdbc(sql_Soursellworker).Tables[0];
                CompareToDataTable(dt_Soursellworker, dt_Targetsellworker, "t_sellworker"); 
 
                string sql_Targetsalespromote = "select salespromote_id, serverpartcode, shopcode,machinecode,salespromote_startdate, salespromote_enddate, " +
                "salespromote_type, salespromote_name,threshold_amount,discount_rate, salespromote_vip,flag,ticket_prefix ," +
                " salespromote_date,salespromote_desc,promotion_id, rtmembership_id,commodity_code from  t_salespromote where commodity_code is not null and salespromote_type is not null and salespromote_startdate is not null and salespromote_enddate is not null";

                string sql_Soursalespromote = "select salespromote_id, serverpartcode, shopcode,machinecode,salespromote_startdate, salespromote_enddate, " +
              "salespromote_type, salespromote_name,threshold_amount,discount_rate, salespromote_vip,flag,ticket_prefix ," +
              " salespromote_date,salespromote_desc,promotion_id, rtmembership_id, commodity_code from  t_salespromote where commodity_code is not null and salespromote_type is not null and salespromote_startdate is not null and salespromote_enddate is not null ";

                DataTable dt_Soursalespromote = SourceDb.QueryOdbc(sql_Targetsalespromote).Tables[0];
                DataTable dt_Targetsalespromote = TargetDb.QueryOdbc(sql_Soursalespromote).Tables[0];
                CompareToDataTable(dt_Soursalespromote, dt_Targetsalespromote, "t_salespromote"); 

                string sql_Targetcommodityex = "select distinct(commodity_barcode), commodityex_id,serverpartcode,commodity_type,commodity_code,commodity_name," +
                    "commodity_rule,commodity_unit,commodity_retailprice,commodity_memberprice,canchangeprice,isvalid,commodity_en,isbulk," +
                    "meteringmethod,downloaddate,flag,businesstype,serverpartshop_id,userdefinedtype_id,commodity_hotkey,container_code,commodity_symbol from t_commodityex where serverpartshop_id=" + Serverpartshop_id + "";

                string sql_Sourcommodityex = "select distinct(commodity_barcode), commodityex_id,serverpartcode,commodity_type,commodity_code,commodity_name," +
                  "commodity_rule,commodity_unit,commodity_retailprice,commodity_memberprice,canchangeprice,isvalid,commodity_en,isbulk," +
                  "meteringmethod,downloaddate,flag,businesstype,serverpartshop_id,userdefinedtype_id,commodity_hotkey,container_code,commodity_symbol from t_commodityex   where serverpartshop_id=" + Serverpartshop_id + "";

                DataTable dt_Sourcommodityex = SourceDb.QueryOdbc(sql_Targetcommodityex).Tables[0];
                DataTable dt_Targetcommodityex = TargetDb.QueryOdbc(sql_Sourcommodityex).Tables[0];
                CompareToDataTable(dt_Sourcommodityex, dt_Targetcommodityex, "t_commodityex");  

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        #region 下载时重构insert（数据带null处理）
        public void DownDataInsert(DataTable _dt, string TableName,DBHelper dbhelper)
        {
            List<string> _List = new List<string>();
            try
            { 
                if (_dt == null)
                {
                    throw new Exception("datatable不可为空！");
                }
                if (string.IsNullOrEmpty(TableName))
                {
                    throw new Exception("表名不可为空！");
                }

                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    string sqlstring = string.Empty;
                    string sqlString = "insert into " + TableName + " ({0}) VALUES ({1})";
                    string Names = string.Empty;
                    string values = string.Empty;
                    foreach (DataColumn c in _dt.Columns)
                    {
                        if (_dt.Rows[i][c.ColumnName] != null && !string.IsNullOrEmpty(_dt.Rows[i][c.ColumnName].ToString()))
                        {
                            Names += (string.IsNullOrEmpty(Names) ? "" : ",") + c.ColumnName;
                            if (IsType(c.DataType, "System.Nullable`1[System.Int16]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Int32]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Int64]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Double]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Decimal]") ||
                               c.DataType == typeof(System.Int16) ||
                               c.DataType == typeof(System.Int32) ||
                                c.DataType == typeof(System.Int64) ||
                                c.DataType == typeof(System.Double) ||
                                c.DataType == typeof(System.Decimal))
                            {
                                values += (string.IsNullOrEmpty(values) ? "" : ",") + _dt.Rows[i][c.ColumnName].ToString();
                            }

                            if (IsType(c.DataType, "System.Nullable`1[System.DateTime]") ||
                               c.DataType == typeof(System.DateTime))
                            {
                                values += (string.IsNullOrEmpty(values) ? "" : ",") + "DateTime('" + _dt.Rows[i][c.ColumnName].ToString() + "')";
                            }
                            if (IsType(c.DataType, "System.String") || c.DataType == typeof(System.String))
                            {
                                values += (string.IsNullOrEmpty(values) ? "" : ",") + "'" + _dt.Rows[i][c.ColumnName].ToString() + "'";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(Names) && !string.IsNullOrEmpty(values))
                    {
                        sqlstring = string.Format(sqlString, Names, values);
                        _List.Add(sqlstring);
                    }
                }
                dbhelper.ExecuteSqlTran(_List);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _List = null;
            }
        }
        #endregion

        #region 比较两个datatable,值不一样的相应的修改下载处理
        public void CompareToDataTable(DataTable SourTable, DataTable TargetTable, string TableName)
        {
            string SelectString = string.Empty;
            List<string> _list = new List<string>();
            DataTable dtInsert = null;
            DataTable drUpdate = null;
            try
            {
                dtInsert = SourTable.Clone();
                drUpdate = SourTable.Clone();
                foreach (DataRow dr in SourTable.Rows)
                {
                    if (TableName == "t_sellworker")
                    {
                        SelectString = "serverpartcode='" + dr["serverpartcode"].ToString() + "' and sellworkercode='" + dr["sellworkercode"].ToString() + "'";
                    }
                    else if (TableName == "t_salespromote")
                    {
                        SelectString = "salespromote_id=" + dr["salespromote_id"].ToString() + "";
                    //    SelectString = "salespromote_startdate='" + dr["salespromote_startdate"].ToString() + "' and salespromote_enddate='" + dr["salespromote_enddate"].ToString() + "' " +
                    //    " and salespromote_type='" + dr["salespromote_type"].ToString() + "' and salespromote_name='" + dr["salespromote_name"].ToString() + "' and commodity_code='" + dr["commodity_code"].ToString() + "'";
                    }
                    else if (TableName == "t_commodityex")
                    {
                        SelectString = "commodity_code='" + dr["commodity_code"].ToString() + "' and commodity_barcode='" + dr["commodity_barcode"].ToString() + "' "; // and serverpartshop_id=" + dr["serverpartshop_id"].ToString() + "
                    }
                    DataRow[] dr2 = TargetTable.Select(SelectString);
                    if (dr2 != null && dr2.Length > 0)
                    {
                        bool flag = false;
                        foreach (DataRow dr3 in dr2)
                        {
                            foreach (DataColumn c in SourTable.Columns)
                            {
                                if (dr3[c.ColumnName] != null && dr[c.ColumnName] != null && dr3[c.ColumnName].ToString() != dr[c.ColumnName].ToString())
                                {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if (flag)
                        {
                            drUpdate.Rows.Add(dr.ItemArray);
                        }
                    }
                    else
                    {
                        dtInsert.Rows.Add(dr.ItemArray);  //添加数据行
                    }
                }

                if (drUpdate != null && drUpdate.Rows.Count > 0)
                {
                    if(TableName=="t_sellworker")
                    {
                          UpdateData(drUpdate, "t_sellworker", new string[] { "serverpartcode", "sellworkercode" }, TargetDb);
                    }
                    else if (TableName == "t_salespromote")
                    {
                        UpdateData(drUpdate, "t_salespromote", new string[] { "salespromote_startdate", "salespromote_enddate", "salespromote_type", "salespromote_name", "commodity_code"}, TargetDb);
                    }
                    else if (TableName == "t_commodityex")
                    {
                        UpdateData(drUpdate, "t_commodityex", new string[] { "serverpartcode", "commodity_barcode", "serverpartshop_id" }, TargetDb);
                    } 
                    MessageBox.Show("更新成功！");
                }
                if (dtInsert != null && dtInsert.Rows.Count > 0)
                {
                    DownDataInsert(dtInsert, TableName, TargetDb);//插入 
                    MessageBox.Show("下载成功！");
                }
               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                dtInsert = null;
                drUpdate = null;
            }
        }
        #endregion 

        #region 下载时重构update语句(处理数据空的情况)
        public void UpdateData(DataTable table, string TableName, string[] keys,DBHelper  dbhelper)
        {
            List<string> _List = null;
            Dictionary<string, string> keyList = null;
            try
            {
                if (_List == null)
                {
                    _List = new List<string>();
                }
                else
                {
                    _List.Clear();
                }

                if (table == null)
                {
                    throw new Exception("行不可为空！");
                }
                if (string.IsNullOrEmpty(TableName))
                {
                    throw new Exception("表名不可为空！");
                }
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (keyList == null)
                    {
                        keyList = new Dictionary<string, string>();
                    }
                    else
                    {
                        keyList.Clear();
                    }
                    string sqlstring = string.Empty;
                    string sqlString = "Update " + TableName + " set {0} where {1}";
                    string values = string.Empty;
                    foreach (DataColumn c in table.Columns)
                    {
                        if (table.Rows[i][c.ColumnName] != null && !string.IsNullOrEmpty(table.Rows[i][c.ColumnName].ToString()))
                        {
                            if (IsType(c.DataType, "System.Nullable`1[System.Int16]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Int32]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Int64]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Double]") ||
                                IsType(c.DataType, "System.Nullable`1[System.Decimal]") ||
                               c.DataType == typeof(System.Int16) ||
                               c.DataType == typeof(System.Int32) ||
                                c.DataType == typeof(System.Int64) ||
                                c.DataType == typeof(System.Double) ||
                                c.DataType == typeof(System.Decimal))
                            {
                                values += ((string.IsNullOrEmpty(values) ? "" : ",") + c.ColumnName + "=" + table.Rows[i][c.ColumnName].ToString());
                                if (keys.Contains(c.ColumnName.ToUpper()) || keys.Contains(c.ColumnName.ToLower()))
                                {
                                    keyList.Add(c.ColumnName, table.Rows[i][c.ColumnName].ToString());
                                }
                            }

                            if (IsType(c.DataType, "System.Nullable`1[System.DateTime]") ||
                               c.DataType == typeof(System.DateTime))
                            {
                                values += ((string.IsNullOrEmpty(values) ? "" : ",") + c.ColumnName + "=" + "DateTime('" + table.Rows[i][c.ColumnName].ToString() + "')");
                                if (keys.Contains(c.ColumnName.ToUpper()) || keys.Contains(c.ColumnName.ToLower()))
                                {
                                    keyList.Add(c.ColumnName, "DateTime('" + table.Rows[i][c.ColumnName].ToString() + "')");
                                }
                            }
                            if (IsType(c.DataType, "System.String") || c.DataType == typeof(System.String))
                            {
                                values += (string.IsNullOrEmpty(values) ? "" : ",") + c.ColumnName + "=" + "'" + table.Rows[i][c.ColumnName].ToString() + "'";
                                if (keys.Contains(c.ColumnName.ToUpper()) || keys.Contains(c.ColumnName.ToLower()))
                                {
                                    keyList.Add(c.ColumnName, "'" + table.Rows[i][c.ColumnName].ToString() + "'");
                                }
                            }
                        }
                    }  
                    if (!string.IsNullOrEmpty(values) && keyList != null && keyList.Count == keys.Length)
                    {
                        string strKeys = string.Empty;
                        foreach (KeyValuePair<string, string> kvp in keyList)
                        {
                            strKeys += ((string.IsNullOrEmpty(strKeys) ? "" : " AND ") + kvp.Key + " = " + kvp.Value);
                        }

                        if (!string.IsNullOrEmpty(strKeys))
                        {
                            sqlstring = string.Format(sqlString, values, strKeys);
                            _List.Add(sqlstring);
                        }

                    }
                }
                dbhelper.ExecuteSqlTran(_List);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _List = null;
                keyList = null;
            }
        }
        #endregion
     

    }
}
